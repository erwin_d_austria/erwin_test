<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Login_model extends My_Model {

	protected $_table_name = "";
	protected $_order_by = "id";
	public $rules = array(
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'required|valid_email'
			),
		'password' => array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required')
	);

	function __construct() {
		parent::__construct();
	}

	/**
	   * 
	   * get user details
	   * used login controler
	   *
	   * @return row
	**/
	public function login() {
		$this->_table_name = 'tbl_employee';
		$this->db->select($this->_table_name.'.id as id');
		$this->db->select($this->_table_name.'.emp_userName as user_name');
		$this->db->select($this->_table_name.'.emp_email as email');

		return $this->get_by(array(
			'emp_email' => $this->input->post('email'),
			'emp_userPassword' => $this->input->post('password')
			), 'row');

	}

	/**
	   * 
	   * get user group level
	   * used login controler
	   *
	   * @param integer $id of employee
	   * @return object
	**/

	public function loggedin() {
		return (bool) $this->session->userdata('loggedin');
	}

	/**
	   * 
	   * get user group level
	   * used in libraries admin
	   *
	   * @param integer $id of employee
	   * @return object
	**/
	public function get_user_group($id) {
		$this->_table_name = 'tbl_employee_group';

		$this->db->select($this->_table_name.'.id as id');
		$this->db->select($this->_table_name.'.employeeDepartmentId as employee_department_id');
		$this->db->select($this->_table_name.'.loginStatusId as login_status');

		return $this->get_by(array(
			$this->_table_name.'.employeeId' => $id
			), 'result'); 
	}

}