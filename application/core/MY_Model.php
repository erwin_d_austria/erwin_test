<?php
/**
* 
*/
class MY_Model extends CI_Model {

	protected $_table_name = "";
	protected $_primary_key = "id";
	protected $_primary_filter = "intval";
	protected $_order_by = NULL;
	public 	$_rules = array();
	// protected $_timestamp = FALSE; 
	protected $_column_like = '';
	protected $_column_like_search = '';
	protected $_column_like_wildcard = 'both';

	function __construct() {
		parent::__construct();
	}

	public function get($id = NULL, $single = FALSE, $limit = 0, $off_set = 0) {

		if($id != NULL) {
			$this->db->like($this->_column_like, $this->_column_like_search, $this->_column_like_wildcard);
			$method = 'result';
		}
		else if($single == 'num_rows') {
			$method = 'num_rows';
		}
		else if($single == 'row') {
			$method = 'row';
		}
		else if($single == 'result') {
			$method = 'result';
		}

		if($this->_order_by != '' && $this->_order_by != NULL) {
			$this->db->order_by($this->_order_by);
		}
		return $this->db->get($this->_table_name, $limit, $off_set)->$method();
	}

	public function get_id($id = NULL, $method = 'result') {

		if($id != NULL) {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_primary_key, $id);
		}
		
		if($this->_order_by != '' && $this->_order_by != NULL) {
			$this->db->order_by($this->_order_by);
		}
		return $this->db->get($this->_table_name)->$method();
	}

	public function get_by($where, $single = FALSE, $limit = 0, $off_set = 0) {
		$this->db->where($where);
		return $this->get(NULL, $single, $off_set, $limit);
	}

	public function save($data, $id = NULL) {

		// Insert 
		if($id == NULL) {
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name);
			$id = $this->db->insert_id();
		}
		// Update
		else {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);
		}

		return $id;

	}
	public function delete($id) {
		$filter = $this->_primary_filter;
		$id = $filter($id);

		if(!$id) {
			return FALSE;
		}

		$this->db->where($this->_primary_key, $id);
		$this->db->delete($this->_table_name);
	}

}