<?php

class MY_Controller extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->data['errors'] = array();

		$this->load->model('Login_model');
		$this->data['user_level'] = $this->Login_model->get_user_group($this->session->userdata('id'));

		$this->data['super_admin'] = 1;
		$this->data['admin'] = 2;
		$this->data['mechanical'] = 3;	
		
	}

	public $_not_allowed = 'Not_allowed';

	public function security_checking($user_group, $expected_status) {
		check_group($this->data['user_level'], $user_group) == TRUE || redirect($this->_not_allowed);
		$security = check_if_admin($this->data['user_level'], $user_group);
		in_array($security, $expected_status)  == TRUE  || redirect($this->_not_allowed);
		return $security;
	}


	public function validation_check() {
		if($this->Nv3d_admin_m->role() != 'admin') { redirect($this->_index, 'refresh'); }
	}
}