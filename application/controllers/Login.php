<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends My_Controller {

	private $_dashboard = 'su_admin_controller';

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->_init();
		$this->data['title'] = config_item('login_title');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('Login_model');	
	}

	private function _init()
	{
		$this->output->set_template('fullwidth');

	}

	public function index()
	{
		$this->Login_model->loggedin() == FALSE || redirect($this->_dashboard, 'refresh');

		var_dump($this->session->userdata('email'));

		if(isset($_POST['email'])) {

			$rules = $this->Login_model->rules;
		
			$this->form_validation->set_rules($rules);
			if($this->form_validation->run() == TRUE) {

				$user = $this->Login_model->login();

				if(count($user)) {
					// Log in user
					$data = array(
						'name' 		=> $user->user_name,
						'email' 	=> $user->email,
						'id' 		=> $user->id,
						'loggedin' 	=> TRUE
					);

					$this->session->set_userdata($data);
					redirect(base_url('su_admin_controller'));
				}
				else {
					$this->session->set_flashdata('error', "<div class='error'>Your email or password was incorrect.<div>");
				}
			}
		}

		$this->load->view('login', $this->data);
		$this->output->set_common_meta('Login | 3DFABLAB SYSTEM', ' ', ' ');
		$this->load->view('login', $this->data );
	}
}