<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="login-form">
				<div class="img-container text-center"><img src="<?php echo base_url('assets/themes/novio/img/login_logo.png'); ?>" class="img-responsive"></div>
				<div class="text-center text-uppercase branding-text">3dfablab phils., inc system</div>
				<?php 
                    echo validation_errors('<span class="error">', '</span> <br>');  
                    echo  $this->session->flashdata('error'); 
                ?>
				<form role="form" action="<?php echo base_url('Login') ?>" method="POST">
					<div class="form-group">
						<div class="icon-addon addon-lg">
							<input type="text" class="form-control nv-input" name="email" placeholder="email">
							<label for="email" class="fa fa-user" rel="tooltip" title="email" ></label>
						</div>
					</div>
					<div class="form-group">
						<div class="icon-addon addon-lg">
							<input type="password" class="form-control nv-input" name="password" placeholder="password">
							<label for="password" class="fa fa-key" rel="tooltip" title="password"></label>
						</div>
					</div>
					<button type="submit" class="btn btn-login-n btn-block text-uppercase">Login</button>
				</form>
			</div>
		</div>
	</div>
</div>